if(Meteor.isClient) {

   Template.places.helpers({
      places : function(){
         return Places.find({});
      }
   });

   Template.place.helpers({
      members : function(team_id){
         return People.find({team_id: this._id});
      },
   });


   Template.places.events({
      'click .segment .column .remove' : function(evt){
         People.update(this._id, {$set:{ team_id: null}});
      }
   });

   Template.place.events({
      'dblclick .segment' : function(evt){
         evt.stopPropagation();
         if(Session.get('person_id')) {
            People.update(Session.get('person_id'), {$set: {team_id: this._id}});
            console.log(this._id);
         }
      }
   })
}
