if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault('counter', 0);


  Template.app.onRendered(function(){

      $('.ui.sticky').sticky({
        context: '#app'
      });

  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
